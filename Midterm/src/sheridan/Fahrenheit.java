package sheridan;

public class Fahrenheit {
	
	public static int fromCelcius (int value) {
		
		int fahrenheit = value * (9/5) + 32; 
		
		return fahrenheit;
	}

}
