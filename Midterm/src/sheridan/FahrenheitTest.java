/*
 * Author Mert Havza 9915398291
 */


package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelciusRegular() {
		
		int fahrenheit = Fahrenheit.fromCelcius(5);
		
		assertTrue("Invalid temparature", fahrenheit == 37);
		
	}
	
	@Test(expected=Error.class)
	public void testFromCelciusException() {
		
		int fahrenheit = Fahrenheit.fromCelcius(-56);
		
		fail("Invalid temparature");
		
	}
	
	@Test
	public void testFromCelciusBoundaryIn() {
		
		int fahrenheit = Fahrenheit.fromCelcius(0);
		System.out.println(fahrenheit);
		assertTrue("Invalid temparature", fahrenheit == 32);
		
	}
	
	@Test(expected=Error.class)
	public void testFromCelciusBoundaryOut() {
		
		int fahrenheit = Fahrenheit.fromCelcius(64);
		
		assertFalse("Invalid temparature", fahrenheit == 96);
		
	}
	
	
}
